package cs.spring.couchbase.SpringCaseStudy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/person")
public class PersonController {
    @Autowired
    private PersonRepository personRepository;

    @PostMapping
    public Person createPerson(@RequestBody Person person){
        return personRepository.save(person);
    }
    @GetMapping("/{id}")
    public Person getPerson(@PathVariable String id) {
        return personRepository.findById(id).orElse(null);
    }
}
